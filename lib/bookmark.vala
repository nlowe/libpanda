using Json;

namespace panda.bookmark{
    
    /**
     * A bookmark for a specific song or artist. Added to stations, can be used to play samples (if songs)
     * or to create new stations or add feedback or start searches
     */
    public class Bookmark : GLib.Object{
        public DateTime created{get; internal set;}
        public string artist{get; internal set;}
        public string music_token{get; internal set;}
        public string artwork_url{get; internal set;}
        public string token{get; internal set;}
    }

    /**
     * A bookmark of a particular Artist
     */    
    public class ArtistBookmark : Bookmark{
        public ArtistBookmark(Json.Object source){
            artist = source.get_string_member("artistName");
            music_token = source.get_string_member("musicToken");
            token = source.get_string_member("bookmarkToken");
            artwork_url = source.get_string_member("artUrl");
            created = new DateTime.from_unix_utc(source.get_object_member("dateCreated").get_int_member("time"));
        }
    }
    
    /**
     * A bookmark of a particular Song
     */
    public class SongBookmark : ArtistBookmark{
        public double sample_gain{get; private set;}
        public string sample_url{get; private set;}
        public string album_name{get; private set;}
        public string song_name{get; private set;}
        
        public SongBookmark(Json.Object source){
            base(source);
            sample_gain = source.get_double_member("sampleGain");
            sample_url = source.get_string_member("sampleUrl");
            album_name = source.get_string_member("albumName");
            song_name = source.get_string_member("songName");
        }
    }
}