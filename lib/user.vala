using Gee;
using Json;
using panda;
using panda.bookmark;
using panda.request.auth;
using panda.request.user;

/**
 * A representation of a user, contianing a list of stations and bookmarks specific to this user
 */
public class panda.User : GLib.Object{
    /** The username (or email address) of the user */
    public string username{get; private set;}
    /** The Web username of this user. Probably what is used if you change your username to something other than your email */
    public string web_username{get; private set;}
    /** The authentication token for the user. It is unknown whether or not this can be cached and if so for how long */
    public string user_auth_token{get; private set;}
    /** The ID of this user */
    public string user_id{get; private set;}
    
    /** A list of stations that this user has created or had shared with them */
    public HashMap<string,Station> stations{get; private set;}
    /** A checksum of the station list, returned by pandora. */
    public string station_list_checksum{get; private set;}
    
    /** A list of bookmarked artists */
    public ArrayList<ArtistBookmark> artist_bookmarks{get; private set;}
    /** A list of bookmarked songs */
    public ArrayList<SongBookmark> song_bookmarks{get; private set;}
    
    /** The JSON source that this object was created from. Use it to access properties that are not yet supported by the API */
    public Json.Object source{get; private set;}
    
    //TODO: How to handle demographics?
    //TODO: Do anything with ad attributes?
    //TODO: Station art url?
    //TODO: How to handle station seeds?
    //TODO: How to handle subscription expiration?
    //TODO: What is a userstate?
    //TODO: Track lifetime stats?
    //TODO: Listening hours?
    
    /** Called whenever the station list gets synced with pandora */
    public signal void station_list_updated();
    /** Called whenever the bookmark lists get synced with pandora */
    public signal void bookmarks_updated();
    
    /**
     * Attempts to log in a user given a username and password combination
     */
    public static async User login(string username, string password, bool include_bookmarks=false, Api api = Api.instance) throws PandoraError{
        var request = new UserLogin();
        request.username = username;
        request.password = password;
        request.return_capped = true;
        request.return_station_list = true;
        request.include_skip_delay = true;
        request.return_is_subscriber = true;
        request.return_has_used_trial = true;
        request.include_user_webname = true;
        request.include_account_message = true;
        
        Json.Object result = yield api.execute_request(request);
        Json.Object bookmarks;
        var user = new User.from_json(result);
        
        api.user = user;
        
        if(include_bookmarks){
            yield user.sync_bookmarks();
        }
        
        return user;
    }
    
    public User.from_json(Json.Object object){
        artist_bookmarks = new ArrayList<ArtistBookmark>();
        song_bookmarks = new ArrayList<SongBookmark>();
        stations = new HashMap<string,Station>();
        username = object.get_string_member("username");
        user_auth_token = object.get_string_member("userAuthToken");
        user_id = object.get_string_member("userId");

        if(object.has_member("stationListResult")){ 
            foreach(var s in object.get_object_member("stationListResult").get_array_member("stations").get_elements()){
                if(s.get_object().get_boolean_member("isQuickMix")){
                    debug("Found QuickMix station");
                    var current = new Station.QuickMixStation.partial(s.get_object());
                    stations[current.station_id] = current;
                }else{
                    var current = new Station.partial(s.get_object());
                    stations[current.station_id] = current;
                }
            }
            station_list_checksum = object.get_object_member("stationListResult").get_string_member("checksum");
        }
        
        if(object.has_member("userWebname")){
            web_username = object.get_string_member("userWebname");
        }
        
        source = object;
    }
    
    /**
     * Sync stations and bookmarks for this user
     */
    public async void sync() throws PandoraError{
        //TODO: Any way to sync other fields?
        yield sync_bookmarks();
        yield sync_stations();
    }
    
    /**
     * Syncs the bookmark lists for this user
     */
    public async void sync_bookmarks(Api api = Api.instance) throws PandoraError{
        Json.Object result = yield api.execute_request(new GetBookmarks());
        
        artist_bookmarks.clear();
        foreach(var b in result.get_array_member("artists").get_elements()){
            artist_bookmarks.add(new ArtistBookmark(b.get_object()));
        }
        
        song_bookmarks.clear();
        foreach(var b in result.get_array_member("songs").get_elements()){
            song_bookmarks.add(new SongBookmark(b.get_object()));
        }
        
        bookmarks_updated();
    }
    
    /**
     * Syncs the station list if forced or if the checksum doesn't match. Please note that this will incomplete all stations,
     * unless you set sync_individual_stations to true.
     */
    public async void sync_stations(bool force=false, bool sync_individual_stations=false, Api api = Api.instance) throws PandoraError{
        if(!force && station_list_checksum != null && station_list_checksum != "" && !(yield stations_need_synced())){
            return;
        }
        
        var request = new GetStationList();
        Json.Object updated_stations = yield api.execute_request(request);
        
        stations.clear();
        foreach(var s in updated_stations.get_array_member("stations").get_elements()){
            if(s.get_object().get_boolean_member("isQuickMix")){
                debug("Found QuickMix station");
                var current = new Station.QuickMixStation.partial(s.get_object());
                stations[current.station_id] = current;
            }else{
                var current = new Station.partial(s.get_object());
                stations[current.station_id] = current;
            }
        }
        station_list_checksum = updated_stations.get_string_member("checksum");
        
        station_list_updated();
    }
    
    /**
     * Determines if the station list needs synced by comparing the local checksum of the stations list and pandora's copy of the checksum
     */
    public async bool stations_need_synced(Api api = Api.instance) throws PandoraError{
        Json.Object result = yield api.execute_request(new GetStationListChecksum());
        return result.get_string_member("checksum") != station_list_checksum;
    }
}