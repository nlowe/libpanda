using Gee;
using Json;

/**
 * A map of audio entries for a track
 */
public class panda.AudioMap : GLib.Object{

    private HashMap<string, AudioEntry> primary_map = new HashMap<string, AudioEntry>();
    private HashMap<string, AudioEntry> additional_map = new HashMap<string, AudioEntry>();

    public AudioMap(Json.Object source){
        foreach(string key in source.get_members()){
            AudioEntry a = new AudioEntry(source.get_object_member(key), key);
            primary_map[key] = a;
        }
    }
    
    public void add_additional_url(string format, string url){
        additional_map[format] = new AudioEntry.from_additional(url);
    }
    
    //TODO: Can we make this an indexor (map["High"], etc)
    public AudioEntry? get_entry(string key){
        if(primary_map.has_key(key)){
            return primary_map[key];
        }else if(additional_map.has_key(key)){
            return additional_map[key];
        }else{
            return null;
        }
    }
    
    /**
     * Checks to see if the specified format is available in the audio map
     */
    public bool format_supported(string fmt){
        return format_primarially_supported(fmt) || format_additionally_supported(fmt);
    }
    
    /**
     * Checks to see if the format is supported in the primary map
     */
    public bool format_primarially_supported(string fmt){
        return primary_map.has_key(fmt);
    }
    
    /**
     * Checks to see if the format is supported in the additional Audio URL map
     */
    public bool format_additionally_supported(string fmt){
        return additional_map.has_key(fmt);
    }
}

public class panda.AudioEntry : GLib.Object{

    public string quality_key{get; private set; default="UNKNOWN";}
    public string bitrate{get; private set; default="UNKNOWN";}
    public string encoding{get; private set; default="UNKNOWN";}
    public string protocol{get; private set; default="UNKNOWN";}
    public string audio_url{get; private set;}

    public AudioEntry(Json.Object source, string quality){
        quality_key = quality;
        bitrate = source.get_string_member("bitrate");
        encoding = source.get_string_member("encoding");
        protocol = source.get_string_member("protocol");
        audio_url = source.get_string_member("audioUrl");
    }
    
    public AudioEntry.from_additional(string audio_url){
        this.audio_url = audio_url;
    }
}