using Json;
using panda;
using panda.request.station;

/**
 * A feedback (or rating) object, used to Thumbs-Up or ban songs on stations
 * Pandora will attempt to use this data to provide you with better music options
 */
public class panda.Feedback : GLib.Object{
        public DateTime created{get; private set;}
        public string   album_art_url{get; private set;}
        public string   music_token{get; private set;}
        public string   song_name{get; private set;}
        public string   artist_name{get; private set;}
        public string   feedback_id{get; private set;}
        public bool     is_positive{get; private set;}
        
        public signal void removed(Feedback f);
        
        public Feedback(Json.Object source){
            created = new DateTime.from_unix_utc(source.get_object_member("dateCreated").get_int_member("time"));
            album_art_url = source.get_string_member("albumArtUrl");
            music_token = source.get_string_member("musicToken");
            song_name = source.get_string_member("songName");
            artist_name = source.get_string_member("artistName");
            feedback_id = source.get_string_member("feedbackId"); 
            is_positive = source.get_boolean_member("isPositive");
        }
        
        /**
         * Removes this feedback object from the station it belongs to
         */
        public async void remove(Api api = Api.instance) throws PandoraError{
            var request = new DeleteFeedback();
            request.feedback_id = feedback_id;
            
            yield api.execute_request(request);
            removed(this);
        }
}