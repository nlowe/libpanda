using Json;
using panda;
using panda.bookmark;
using panda.request.track;

/**
 * A representation of a track. These are returned in playlists and can be used
 * as sources for bookmarks and station variation as well as station seeds.
 */
public class panda.Track : GLib.Object{
    
    /** Unique Identifier for this track*/
    public string identity{get; private set;}
    /** The Token (or id) of this track */
    public string token{get; private set;}
    /** The name of the artist of this track */
    public string artist{get; private set;}
    /** The name of the album this track belongs to */
    public string album{get; private set;}
    /** A URL to the image of the album artwork */
    public string album_artwork_url{get; private set;}
    /** A map of audio formats for this track */
    public AudioMap audio_map{get; private set;}
    /** The name of this track */
    public string name{get; private set;}
    /** Whether or not this track has been rated with a thumbs-up*/
    public bool   thumbs_up{get; private set;}
    /** The pre-amp gain that should be applied for this track? */
    public double gain{get; private set;}
    
    /** The JSON source that this object was created from. Use it to access properties that are not yet supported by the API */
    public Json.Object source{get; private set;}
    
    public Track(Json.Object source){
        identity = source.get_string_member("songIdentity");
        token = source.get_string_member("trackToken");
        artist = source.get_string_member("artistName");
        album = source.get_string_member("albumName");
        album_artwork_url = source.get_string_member("albumArtUrl");
        audio_map = new AudioMap(source.get_object_member("audioUrlMap"));
        
        //TODO: support additional audio
        
        name = source.get_string_member("songName");
        thumbs_up = source.get_int_member("songRating") == 1 ? true : false;
        gain = source.get_double_member("trackGain");
        //TODO: Optional parameters
        
        this.source = source;
    }
    
    /**
     * Gets the explanation of why pandora chose to play this track
     */
    public async string explain(Api api = Api.instance) throws PandoraError{
        debug(@"Fetching explanation for $(to_string()) [$token]");
        string result = "";
    
        var request = new ExplainTrack();
        request.track_token = token;
        
        Json.Object? response = yield api.execute_request(request);
        foreach(var node in response.get_array_member("explanations").get_elements()){
            result += node.get_object().get_string_member("focusTraitName");
            result += ", ";
        }
        result = result.chomp();
        
        return result[0:result.char_count()-2];
    }
    
    /**
     * Bookmarks this track and adds it to the user's bookmarks
     */
    public async SongBookmark bookmark(Api api = Api.instance) throws PandoraError{
        var request = new panda.request.bookmark.AddSongBookmark();
        request.track_token = token;
        
        Json.Object response = yield api.execute_request(request);
        
        var b = new SongBookmark(response);
        api.user.song_bookmarks.add(b);
        
        return b;
    }
    
    /**
     * Bookmarks the artist of this track and adds it to the user's boomarks
     */
    public async ArtistBookmark bookmark_artist(Api api = Api.instance) throws PandoraError{
        var request = new panda.request.bookmark.AddArtistBookmark();
        request.track_token = token;
        
        Json.Object response = yield api.execute_request(request);
        
        var b = new ArtistBookmark(response);
        api.user.artist_bookmarks.add(b);
        return b;
    }
    
    /**
     *  Returns the name of this track in the format "$NAME by $ARTIST ($ALBUM)"
     */
    public string to_string(){
        return @"$name by $artist ($album)";
    }
}