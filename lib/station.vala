using Gee;
using Json;
using panda;
using panda.request.station;
using panda.request.user;

//TODO: Support Quick Mix stations
/**
 * A representation of a Station. Contains feedback, seeds, and the playlist
 *
 * By default, stations automatically fetch a new playlist after a track has finished playing and the
 * current playlist size is below TRACK_AUTOGROW_THRESHOLD
 */
public class panda.Station : GLib.Object{

    /** The ID of the station. Currently the same as the stationToken */
    public string station_id{get; protected set;}
    /** The token of the station. Currently the same as the stationId */
    public string station_token{get; protected set;}
    /** Whether or not this station is shared. Shared stations cannot have seeds added to them */
    public bool   shared{get; protected set;}
    /** The name of this station */
    public string name{get; protected set;}
    /** The date that this station was created */
    public DateTime created{get; protected set;}
    /** A URL pointing to a picture that should be displayed to describe this station*/
    public string artwork_url{get; protected set;}
    
    /** The playlist object, which keeps track of upcomming tracks, the current track, and track history */
    public Playlist playlist{get; protected set;}
    /** A list of positive feedback items */
    public ArrayList<Feedback> thumbs_up{get; protected set;}
    /** A list of negative feedback items */
    public ArrayList<Feedback> thumbs_down{get; protected set;}
    
    /** Whether or not this station is a full or partial station */
    public bool completed{get; protected set;}
    
    public enum FEEDBACK_CHANGE_OPERATION{ADD,MODIFY,DELETE} //TODO: Is modify applicable?
    /** Called whenever feedback is added to or removed from this station */
    public signal void feedback_changed(FEEDBACK_CHANGE_OPERATION op);
    
    /** The JSON source that this object was created from. Use it to access properties that are not yet supported by the API */
    public Json.Object source{get; protected set;}
    
    //TODO: Station Seeds?
    
    /**
     * Creates a full station from the specified JSON Object
     */
    public Station(Json.Object source){
        this.partial(source);
        load_full(source);
    }
    
    /**
     * Creates a partial station from the specified JSON object
     */
    public Station.partial(Json.Object source){
        playlist = new Playlist(this);
        thumbs_up = new ArrayList<Feedback>();
        thumbs_down = new ArrayList<Feedback>();
        load_partial(source);
    }
    
    protected virtual void load_partial(Json.Object source){
        debug("STATION_LOAD_PARTIAL");
        completed = false;
        
        station_id = source.get_string_member("stationId");
        station_token = source.get_string_member("stationToken");
        shared = source.get_boolean_member("isShared");
        created = new DateTime.from_unix_utc((int64)source.get_object_member("dateCreated").get_int_member("time"));
        name = source.get_string_member("stationName");
        
        this.source = source;
    }

    protected virtual void load_full(Json.Object source){
        debug("STATION_LOAD_FULL");
        artwork_url = source.get_string_member("artUrl");
        Json.Object feedback_list = source.get_object_member("feedback");
        foreach(Json.Node feedback in feedback_list.get_array_member("thumbsUp").get_elements()){
            register_feedback(new Feedback(feedback.get_object()));
        }
        
        foreach(Json.Node feedback in feedback_list.get_array_member("thumbsDown").get_elements()){
            register_feedback(new Feedback(feedback.get_object()));
        }
        
        this.source = source;
        //TODO: seeds
        completed = true;
    }
    
    /**
     *  Updates the metadata for this station
     */
    public async void sync(Api api = Api.instance) throws PandoraError{
        var request = new GetStation();
        request.station_token = station_token;
        request.include_extended_attributes = true;
        
        Json.Object result = yield api.execute_request(request);
        
        thumbs_up.clear();
        thumbs_down.clear();
        
        load_partial(result);
        load_full(result);
        
        completed = true;
    }
    
    /**
     * Fetches additional tracks for this station's playlist and adds them to the playlist.
     */
    public async void fetch_new_playlist(string formats = "HTTP_64_AAC", Api api = Api.instance) throws PandoraError{
        //TODO: Ad Handling here?
        var request = new GetPlaylist();
        request.station_token = station_token;
        request.additional_audio_url = formats;
        
        Json.Object result = yield api.execute_request(request);
        foreach(var obj in result.get_array_member("items").get_elements()){
            if(obj.get_object().has_member("adToken")){
              debug("Skipping entry because it looks like just an adToken");
            }else{
              playlist.push(new Track(obj.get_object()));
            }
        }
    }
    
    /**
     * Adds feedback about a track to this station. If no track token is specified, the current track is implied
     */
    public virtual async void add_feedback(bool thumbs_up, string token=null, Api api = Api.instance) throws PandoraError{
        if(token == null){
            token = playlist.current_track.token;
        }
        
        assert(token != null);
        
        var request = new AddFeedback();
        request.station_token = station_token;
        request.track_token = token;
        request.is_positive = thumbs_up;
        
        Json.Object response = yield api.execute_request(request);
        Feedback f = new Feedback(response);
        
        register_feedback(f);
        feedback_changed(FEEDBACK_CHANGE_OPERATION.ADD);
    }
    
    protected virtual void register_feedback(Feedback f){
        f.removed.connect(on_feedback_removed);
        if(f.is_positive){
            this.thumbs_up.add(f);
        }else{
            this.thumbs_down.add(f);
        }
    }
    
    /**
     * A callback connected to Feedback objects and called when they are deleted in the API
     */
    public void on_feedback_removed(Feedback f){
        if(f.is_positive){
            thumbs_up.remove(f);
        }else{
            thumbs_down.remove(f);
        }
        feedback_changed(FEEDBACK_CHANGE_OPERATION.DELETE);
    }
    
    public class QuickMixStation : panda.Station{
    
        /** A list of station id's that comprise the quick mix station */
        public ArrayList<string> mixins{get; private set;}
    
        public QuickMixStation(Json.Object source){
            this.partial(source);
            load_full(source);
        }
        
        public QuickMixStation.partial(Json.Object source){
            base.partial(source);
            /*GLib.Object(); //Without this, the compiler throws:   error: unable to chain up to base constructor requiring arguments
            playlist = new Playlist(this);
            thumbs_up = new ArrayList<Feedback>();
            thumbs_down = new ArrayList<Feedback>();
            load_partial(source);*/
        }
        
        protected override void load_partial(Json.Object source){
            debug("QUICKMIX_LOAD_PARTIAL");
            base.load_partial(source);
            mixins = new ArrayList<string>();
            foreach(var j in source.get_array_member("quickMixStationIds").get_elements()){
                mixins.add(j.get_string());
            }
        }
        
        protected override void load_full(Json.Object source){
            debug("QUICKMIX_LOAD_FULL");
            //TODO: Do we need anything else?
            artwork_url = source.get_string_member("artUrl");
            this.source = source;
            completed = true;
        }
        
        protected override async void add_feedback(bool thumbs_up, string token=null, Api api = Api.instance) throws PandoraError{
            warning("You cannot add feedback to the QuickMix station!");
        }
        protected override void register_feedback(Feedback f){
            warning("You cannot register feedback on the QuickMix station!");
        }
        
        public async void add_station_to_quickmix(Station s, Api api = Api.instance) throws PandoraError requires(s.station_id != this.station_id && !mixins.contains(s.station_id)){
            string[] new_mixins = new string[mixins.size+1];
            for(int i=0; i<mixins.size; i++){
                new_mixins[i] = mixins[i];
            }
            new_mixins[mixins.size] = s.station_id;
            
            var request = new SetQuickMix();
            request.quick_mix_station_ids = new_mixins;
            
            yield api.execute_request(request);
        }
        
        public async void remove_station_from_quickmix(Station s) throws PandoraError requires(mixins.contains(s.station_id)){
            yield remove_station_from_quickmix_by_id(s.station_id);
        }
        
        public async void remove_station_from_quickmix_by_id(string id, Api api = Api.instance) throws PandoraError requires(mixins.contains(id)){
            mixins.remove(id);
            
            string[] new_mixins = new string[mixins.size];
            for(int i=0; i<mixins.size; i++){
                new_mixins[i] = mixins[i];
            }
            
            var request = new SetQuickMix();
            request.quick_mix_station_ids = new_mixins;
            
            try{
                yield api.execute_request(request);
            }catch(PandoraError e){
                //restore the mixins to a known good state before raising the error
                mixins.add(id);
                throw e;
            }
        }
    }
}