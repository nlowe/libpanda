using Gee;
using Json;
using Soup;
using panda.crypto;
using panda.request;
using panda.request.auth;

/**
 * The Main object used for interacting with the Pandora JSON Web API
 *
 * To initiate the connection, create a new instance of this object and call <code>init_connection()</code>
 */
public class panda.Api : GLib.Object{
    public static Api instance{get; private set;}

    /**
     * The API Entry point. Typically either tuner.pandora.com/services/json/? or internal-tuner.pandora.com/services/json/?
     * Do not prepend HTTP:// or HTTPS://
     */
    public string TUNER_URL{get; private set;}
    
    /** The version of the API and Protocal you intend to use. Currently the only known version is '5'*/
    public static const string PROTOCOL_VERSION = "5";
    
    /** The username used when calling auth.partnerLogin, used to obtain the partner ID and partner auth token */
    public string PARTNER_AUTH_USERNAME{get; private set;}
    /** The password used when calling auth.partnerLogin, used to obtain the partner ID and partner auth token */
    public string PARTNER_AUTH_PASSWORD{get; private set;}
    /** The device id used when calling auth.partnerLogin, used to obtain the partner ID and partner auth token */
    public string PARTNER_DEVICE_ID{get; private set;}
    /** The decryption token used to decrypt encrypted requests recieved from pandora. Dependent on what partner you choose */
    public string PARTNER_DECRYPT_TOKEN{get; private set;}
    /** The encryption token used to encrypt requests sent to pandora. Dependent on what partner you choose */
    public string PARTNER_ENCRYPT_TOKEN{get; private set;}
    
    /** The partner ID recieved from the auth.partnerLogin request */
    public string? PARTNER_ID{get; private set;}
    /** The partner auth token recieved from the auth.partnerLogin request */
    public string? PARTNER_AUTH_TOKEN{get; set;}
    
    /** The default user that the API is currently tracking. Defaults to the last logged in user*/
    public User? user{get; set;}
    
    /** The time difference between local time and the time on pandora's servers. Used to calculate the syncTime parameter */
    public int64 TIME_OFFSET{get; private set; default=0;}
    
    /** The time that the auth.partnerLogin request was made. Used to calculate the syncTime parameter */
    public int64 API_INIT_TIME{get; private set; default=0;}
    
    /** Whether or not auth.partnerLogin was successful */
    public bool CONNECTED{get; private set;}
    
    public Blowfish encrypter{get; private set;}
    public Blowfish decrypter{get; private set;}
    
    private Soup.Session session;
    
    /** A signal called when the API Connection to pandora has been initiated */
    public signal void connected();
    
    private Api(){
        TUNER_URL = "tuner.pandora.com/services/json/";
        session = new Session();
        Api.instance = this;
    }
    
    /**
     * Connect to pandora as a custom device. You need to provide all the needed parameters
     */
    public Api.as_custom(string tuner, string p_auth_u, string p_auth_p, string p_dev_id, string p_decrypt_t, string p_encrypt_t){
        this();
        TUNER_URL = tuner;
        PARTNER_AUTH_USERNAME = p_auth_u;
        PARTNER_AUTH_PASSWORD = p_auth_p;
        PARTNER_DEVICE_ID = p_dev_id;
        PARTNER_DECRYPT_TOKEN = p_decrypt_t;
        PARTNER_ENCRYPT_TOKEN = p_encrypt_t;
    }
    
    /**
     * Connect to pandora as an Android device
     */
    public Api.as_android(){
        this();
        PARTNER_AUTH_USERNAME = "android";
        PARTNER_AUTH_PASSWORD  = "AC7IBG09A3DTSYM4R41UJWL07VLN8JI7";
        PARTNER_DEVICE_ID = "android-generic";
        PARTNER_DECRYPT_TOKEN = "R=U!LH$O2B#";
        PARTNER_ENCRYPT_TOKEN = "6#26FRL$ZWD";
    }
    
    /**
     * Connect to pandora as an iOS Device
     */
    public Api.as_ios(){
        this();
        PARTNER_AUTH_USERNAME = "iphone";
        PARTNER_AUTH_PASSWORD  = "P2E4FC0EAD3*878N92B2CDp34I0B1@388137C";
        PARTNER_DEVICE_ID = "IP01";
        PARTNER_DECRYPT_TOKEN = "20zE1E47BE57$51";
        PARTNER_ENCRYPT_TOKEN = "721^26xE22776";
    }
    
    /**
     * Connect to pandora as a PalmOS device
     */
    public Api.as_palm(){
        this();
        PARTNER_AUTH_USERNAME = "palm";
        PARTNER_AUTH_PASSWORD  = "IUC7IBG09A3JTSYM4N11UJWL07VLH8JP0";
        PARTNER_DEVICE_ID = "pre";
        PARTNER_DECRYPT_TOKEN = "E#U$MY$O2B=";
        PARTNER_ENCRYPT_TOKEN = "%%526CBL$ZU3"; //%% is to escape the percent sign
    }
    
    /**
     * Connect to pandora as a WindowsMobile device
     */
    public Api.as_winmo(){
        this();
        PARTNER_AUTH_USERNAME = "winmo";
        PARTNER_AUTH_PASSWORD  = "ED227E10a628EB0E8Pm825Dw7114AC39";
        PARTNER_DEVICE_ID = "VERIZON_MOTOQ9C";
        PARTNER_DECRYPT_TOKEN = "R=U!LH$O2B#";
        PARTNER_ENCRYPT_TOKEN = "6#26FRL$ZWD";
    }
    
    /**
     * Connect to pandora as the Desktop (Air) client
     */
    public Api.as_desktop(){
        warning("Emulating the Desktop (Air) client requires a pandora one subscription. You won't be able to get playlists without one!");
        this();
        TUNER_URL = "internal-tuner.pandora.com/services/json/";
        PARTNER_AUTH_USERNAME = "pandora one";
        PARTNER_AUTH_PASSWORD  = "TVCKIBGS9AO9TSYLNNFUML0743LH82D";
        PARTNER_DEVICE_ID = "D01";
        PARTNER_DECRYPT_TOKEN = "U#IO$RZPAB%%VX2"; //%% is to escape percent sign
        PARTNER_ENCRYPT_TOKEN = "2%%3WCL*JU$MP]4"; //%% is to escape percent sign
    }
    
    /**
     * Connect to pandora as a Windows Gadget
     */
    public Api.as_vista(){
        this();
        TUNER_URL = "internal-tuner.pandora.com/services/json/";
        PARTNER_AUTH_USERNAME = "windowsgadget";
        PARTNER_AUTH_PASSWORD  = "EVCCIBGS9AOJTSYMNNFUML07VLH8JYP0";
        PARTNER_DEVICE_ID = "WG01";
        PARTNER_DECRYPT_TOKEN = "E#IO$MYZOAB%%FVR2"; //%% is to escape percent sign
        PARTNER_ENCRYPT_TOKEN = "%%22CML*ZU$8YXP[1"; //%% is to escape percent sign
    }
    
    /**
     * Initiate a connection to the API. This must be called before any other requests can be made
     */
    public async void init_connection() throws PandoraError{
        debug("PANDA: Connecting to Pandora");
        encrypter = new Blowfish(PARTNER_ENCRYPT_TOKEN);
        decrypter = new Blowfish(PARTNER_DECRYPT_TOKEN);
    
        var request = new PartnerLogin();
        request.username = PARTNER_AUTH_USERNAME;
        request.password = PARTNER_AUTH_PASSWORD;
        request.device_model = PARTNER_DEVICE_ID;
        request.version = PROTOCOL_VERSION;
        
        Json.Object? result;
        API_INIT_TIME = now();
        try{
            result = yield execute_request(request);
        }catch(PandoraError e){
            throw e;
        }
        
        string s_time = decrypter.decrypt(result.get_string_member("syncTime"));
        
        string[] buff = new string[s_time.char_count()];
        
        int index = 0;
        unichar c;
        for(int i=0; s_time.get_next_char(ref i, out c);){
            buff[index] = c.to_string();
            index++;
        }
        
        string slice = string.joinv("", buff[4:14]);
        TIME_OFFSET = int64.parse(slice);
        
        debug(@"Decrypted Sync Time is $(TIME_OFFSET.to_string()), buffer was $slice and raw s_time was $s_time");
        if(TIME_OFFSET < (API_INIT_TIME-10000)){
            GLib.error("Unable to decode the syncTime. Blame vala's handling of strings :(");
        }
        PARTNER_ID = result.get_string_member("partnerId");
        PARTNER_AUTH_TOKEN = result.get_string_member("partnerAuthToken");
        CONNECTED = true;
        connected();
    }
    
    /**
     * Executes an API request. If the request is anything other than 'auth.partnerLogin', you must already be connected to the API
     */
    public async Json.Object? execute_request(panda.request.Request requestInstance) throws PandoraError{
        if(requestInstance.METHOD_NAME != "auth.partnerLogin"){
            assert(CONNECTED);
        }
        var url = get_api_url(requestInstance);
        var msg = new Message.from_uri("POST", url);
        var gen = new Generator();
        var root = new Json.Node(NodeType.OBJECT);
        var obj = new Json.Object();
        root.set_object(obj);
        gen.set_root(root);
        
        requestInstance.pack(ref obj);
        if(user != null && user.user_auth_token != null)
            obj.set_string_member("userAuthToken", user.user_auth_token);
        else if(PARTNER_AUTH_TOKEN != null)
            obj.set_string_member("partnerAuthToken", PARTNER_AUTH_TOKEN);
        if(CONNECTED)
            obj.set_int_member("syncTime", (int)calculate_sync_time());
        
        string payload = gen.to_data(null);
        
        debug(@"Execute $(requestInstance.METHOD_NAME) at $(url.to_string(false))");
        debug(@"Unencrypted Payload: $payload");
        
        if(requestInstance.ENCRYPTED){
            payload = encrypter.encrypt(payload);
        }
        
        msg.request_body.append_take(payload.data);
        
        try{
            InputStream response_stream = yield session.send_async(msg);
            uint8[] buffer = new uint8[1024];
            ssize_t count = 0;
            do{
                count = response_stream.read(buffer);
                if(count <= 0) break;
                
                msg.response_body.append_take(buffer[0:count]);
                buffer = new uint8[1024];
            }while(count > 0);
        }catch(GLib.Error e){
            error(@"LIBPANDA: An error occurred while communicating with pandora: $(e.message)");
        }
        
        try{
            var parser = new Json.Parser();
            string resp = (string) msg.response_body.flatten().data;
            parser.load_from_data(resp, -1);
            Json.Object? result = parser.get_root().get_object();
            
            if(result == null){
                return null;
            }
            
            string stat = result.get_string_member("stat");
            debug(@"[$stat]: $resp");
            if(stat != "ok"){
                string message = result.get_string_member("message");
                int code = (int)result.get_int_member("code");
                warning(@"Encountered an error when communicating with pandora: [$code] $message");
                switch(code){
                    case 0:    throw new PandoraError.INTERNAL_ERROR("An internal error has occurred.");
                    case 1:    throw new PandoraError.MAINTENANCE_MODE("Pandora is undergoing maintainence.");
                    case 2:    throw new PandoraError.URL_PARAM_MISSING_METHOD("A url parameter is missing. Contact the developer.");
                    case 3:    throw new PandoraError.URL_PARAM_MISSING_AUTH_TOKEN("A url parameter is missing, specifically the Auth Token. Contact the developer.");
                    case 4:    throw new PandoraError.URL_PARAM_MISSING_PARTNER_ID("A url parameter is missing, specifically the Partner ID. Contact the developer.");
                    case 5:    throw new PandoraError.URL_PARAM_MISSING_USER_ID("A url parameter is missing, specifically the User ID. Contact the developer.");
                    case 6:    throw new PandoraError.SECURE_PROTOCOL_REQUIRED("This call must be made over HTTPS. Contact the Developer.");
                    case 7:    throw new PandoraError.CERTIFICATE_REQUIRED("A certificate is required.");
                    case 8:    throw new PandoraError.PARAMETER_TYPE_MISMATCH("The type of a prameter was incorrect. Contact the developer.");
                    case 9:    throw new PandoraError.PARAMETER_MISSING("A parameter is missing. Contact the developer.");
                    case 10:   throw new PandoraError.PARAMETER_VALUE_INVALID("The value of a parameter is invalid. Contact the developer.");
                    case 11:   throw new PandoraError.API_VERSION_NOT_SUPPORTED("The API version has changed. Contact the developer.");
                    case 12:   throw new PandoraError.LICENSING_RESTRICTIONS("Pandora is not available in your country, probably due to some MPAA/RIAA B/S. Complain to your country president/leader/congresperson/etc.");
                    case 13:   throw new PandoraError.INSUFFICIENT_CONNECTIVITY("Your SYNC time is probably incorrect. Try restarting the application.");
                    case 14:   throw new PandoraError.UNKNOWN_METHOD_NAME_MAYBE("That method doesn't exist. Contact the developer.");
                    case 15:   throw new PandoraError.WRONG_PROTOCOL_MAYBE("The protocol is probably incorrect. Contact the developer.");
                    case 1000: throw new PandoraError.READ_ONLY_MODE("Pandora is in Read-Only mode. Changes cannot be made at this time.");
                    case 1001: throw new PandoraError.INVALID_AUTH_TOKEN("Your session has expired. Please log-in again.");
                    case 1002: throw new PandoraError.INVALID_PARTNER_LOGIN("Something went wrong with the authentication. Contact the developer.");
                    case 1003: throw new PandoraError.LISTENER_NOT_AUTHORIZED("Pandora One subscription or Trial expired. Your account may also be suspended.");
                    case 1004: throw new PandoraError.USER_NOT_AUTHORIZED("Your account is not authorized to perform that action.");
                    case 1005: throw new PandoraError.MAX_STATIONS_REACHED("The maximum ammount of stations for your account has been reached.");
                    case 1006: throw new PandoraError.STATION_DOES_NOT_EXIST("The requested station does not exist.");
                    case 1007: throw new PandoraError.COMPLIMENTARY_PERIOD_ALREADY_IN_USE("Your account already has a complementary period active.");
                    case 1008: throw new PandoraError.CALL_NOT_ALLOWED("You cannot add feedback to a shared station.");
                    case 1009: throw new PandoraError.DEVICE_NOT_FOUND("The requested device was not found. Contact the developer.");
                    case 1010: throw new PandoraError.PARTNER_NOT_AUTHORIZED("The partner credentials are incorrect. Contact the developer.");
                    case 1011: throw new PandoraError.INVALID_USERNAME("The username is incorrect.");
                    case 1012: throw new PandoraError.INVALID_PASSWORD("The password is incorrect.");
                    case 1013: throw new PandoraError.USERNAME_ALREADY_EXISTS("That username already exists.");
                    case 1014: throw new PandoraError.DEVICE_ALREADY_ASSOCIATED_TO_ACCOUNT("The target device has already been associated with an account.");
                    case 1015: throw new PandoraError.UPGRADE_DEVICE_MODEL_INVALID("The device model was invalid. Contact the developer.");
                    case 1018: throw new PandoraError.EXPLICIT_PIN_INCORRECT("The explicit pin was incorrect.");
                    case 1020: throw new PandoraError.EXPLICIT_PIN_MALFORMED("The explicit pin format was incorrect. Contact the developer.");
                    case 1023: throw new PandoraError.DEVICE_MODEL_INVALID("The provided device model was invalid. Contact the developer.");
                    case 1024: throw new PandoraError.ZIP_CODE_INVALID("The provided zip code was invalid.");
                    case 1025: throw new PandoraError.BIRTH_YEAR_INVALID("The provided birth year was invalid.");
                    case 1026: throw new PandoraError.BIRTH_YEAR_TOO_YOUNG("Pandora thinks you are a baby. (AKA: You're too young to be using this.)");
                    case 1027: throw new PandoraError.INVALID_COUNTRY_CODE("The provided country code was incorrect.");
                    case 1028: throw new PandoraError.INVALID_GENDER("Pandora says your gender is invalid.");
                    case 1034: throw new PandoraError.DEVICE_DISABLED("The provided device has been marked as disabled. Contact the developer.");
                    case 1035: throw new PandoraError.DAILY_TRIAL_LIMIT_REACHED("The daily limit has been reached for this trial.");
                    case 1036: throw new PandoraError.INVALID_SPONSOR("The provided sponsor was invalid. Contact the developer.");
                    case 1037: throw new PandoraError.USER_ALREADY_USED_TRIAL("You have already used your trial.");
                    case 1039: throw new PandoraError.PLAYLIST_EXCEEDED("Too many requests for a new playlist have been made. Try again in an hour or so.");
                    default: throw new PandoraError.INTERNAL_ERROR("An internal error has occurred.");
                }
            }
            
            return result.get_object_member("result");
        }catch(PandoraError e){
            throw e;
        }catch(GLib.Error e){
            error("An error occurred while trying to decode the response payload: %s", e.message);
        }
    }
    
    /**
     * Gets the complete request URL for a given request
     */
    public URI get_api_url(panda.request.Request request){
        URI api_base = new URI((request.TLS ? "https://" : "http://") + TUNER_URL);
        
        HashTable<string,string> query_params = new HashTable<string,string>(str_hash,str_equal);
        
        query_params["method"] = request.METHOD_NAME;
        
        if(user != null && user.user_auth_token != null){
            query_params["auth_token"] = user.user_auth_token;
        }else if(PARTNER_AUTH_TOKEN != null){
            query_params["auth_token"] = PARTNER_AUTH_TOKEN;
        }
        if(PARTNER_ID != null){
            query_params["partner_id"] = PARTNER_ID;
        }
        if(user != null && user.user_id != null){
            query_params["user_id"] = user.user_id;
        }
        
        api_base.set_query_from_form(query_params);
        return api_base;
    }

    /**
     * Calculates the syncTime parameter
     */
    public int64 calculate_sync_time(){
        debug(@"$TIME_OFFSET + ($(now())-$API_INIT_TIME) = $(TIME_OFFSET + (now() - API_INIT_TIME))");
        return TIME_OFFSET + (now() - API_INIT_TIME);
    }
    
    private int64 now(){ return new DateTime.now_local().to_unix(); }
}