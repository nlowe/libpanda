using Json;

namespace panda.request.user{

    /**
     * Checks if the user is subscribed or if they can subscribe to Pandora One.
     *
     * Can be useful to determine which partner password to use.
     */
    public class CanSubscribe : panda.request.Request{
        construct{ METHOD_NAME="user.canSubscribe"; }
        
        public string iap_vendor{get; set;}
        
        public override void pack(ref Json.Object root){
            if(iap_vendor != null) root.set_string_member("iapVendor", iap_vendor);
        }
    }
    
    /**
     * Change the settings for the user.
     *
     * The following settings may be changed:
     * Name	                                | Type
     * -------------------------------------|--------
     * gender	                            | string
     * birthYear	                        | int
     * zipCode	                            | string
     * isProfilePrivate                 	| boolean
     * enableComments	                    | boolean
     * emailOptIn	                        | boolean
     * emailComments	                    | boolean
     * emailNewFollowers	                | boolean
     * isExplicitContentFilterEnabled	    | boolean
     * isExplicitContentFilterPINProtected	| boolean
     * currentUsername	                    | string
     * currentPassword	                    | string
     * newUsername	                        | string
     * newPassword	                        | string
     * facebookAutoShareEnabled	            | boolean
     * autoShareTrackPlay	                | boolean
     * autoShareLikes	                    | boolean
     * autoShareFollows	                    | boolean
     * facebookSettingChecksum	            | boolean
     * userInitiatedChange              	| boolean
     */
    public class ChangeSettings : panda.request.Request{
        construct{ METHOD_NAME="user.changeSettings"; }
        
        public string iap_vendor{get; set;}
        
        public override void pack(ref Json.Object root){
            //TODO: Figure out packing implementation
        }
    }
    
    /**
     * Submits a request to create a new user. The username should be the user's email address
     */
    public class CreateUser : panda.request.Request{
        construct{ METHOD_NAME="user.createUser"; }
        
        public string username{get; set;}
        public string password{get; set;}
        public string gender{get; set;}
        public int    birth_year{get; set;}
        public int    zip_code{get; set;}
        public bool   email_opt_in{get; set;}
        public string country_code{get; set;}
        public string account_type{get; set; default="registered";}
        public string registered_type{get; set; default="user";}
        public bool   include_pandora_one_info{get; set;}
        public bool   include_account_message{get; set;}
        public bool   collect_track_lifetime_stats{get; set;}
        public bool   xplatform_ad_capable{get; set;}
        public bool   include_facebook{get; set;}
        public bool   include_googleplay{get; set;}
        public bool   include_show_user_recommendations{get; set;}
        public bool   include_advertiser_attributes{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_string_member("username", username);
            root.set_string_member("password", password);
            root.set_string_member("gender", gender);
            root.set_int_member("birthYear", birth_year);
            root.set_int_member("zipCode", zip_code);
            root.set_boolean_member("emailOptIn", email_opt_in);
            root.set_string_member("countryCode", country_code);
            root.set_string_member("accountType", account_type);
            root.set_string_member("registeredType", registered_type);
            root.set_boolean_member("includePandoraOneInfo", include_pandora_one_info);
            root.set_boolean_member("includeAccountMessage", include_account_message);
            root.set_boolean_member("collectTrackLifetimeStats", collect_track_lifetime_stats);
            root.set_boolean_member("xplatformAdCapable", xplatform_ad_capable);
            root.set_boolean_member("includeFacebook", include_facebook);
            root.set_boolean_member("includeGoogleplay", include_googleplay);
            root.set_boolean_member("includeShowUserRecommendations", include_show_user_recommendations);
            root.set_boolean_member("includeAdvertiserAttributes", include_advertiser_attributes); 
        }
    }
    
    /**
     * Sends a password reset email to the specified username
     */
    public class EmailPassword : panda.request.Request{
        construct{ METHOD_NAME="user.emailPassword"; }
        
        public string username{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_string_member("username", username);
        }
    }
    
    /**
     * Gets a list of the user's bookmarked songs and artists
     */
    public class GetBookmarks : panda.request.Request{
        construct{ METHOD_NAME="user.getBookmarks"; }
        
        public override void pack(ref Json.Object root){}
    }
    
    /**
     * Gets a list of the user's settings
     */
    public class GetSettings : panda.request.Request{
        construct{ METHOD_NAME="user.getSettings"; }
        
        public bool include_facebook{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_boolean_member("includeFacebook", include_facebook);
        }
    }
    
    /**
     * Gets a list of checksums of the user's stations. Used to check if a station was modified by another device.
     *
     * This request has no parameters.
     */
    public class GetStationListChecksum : panda.request.Request{
        construct{ METHOD_NAME="user.getStationListChecksum"; }
        
        public override void pack(ref Json.Object root){}
    }
    
    /**
     * Gets a list of the user's stations. Currently stationId and stationToken are the same.
     *
     * The quick mix station also contains a list of stations included in the mix.
     */
    public class GetStationList : panda.request.Request{
        construct{ METHOD_NAME="user.getStationList"; }
        
        public bool   include_station_art_url{get; set;}
        public string station_art_size{get; set; default="W130H130";}
        public bool   include_ad_attributes{get; set;}
        public bool   include_station_seeds{get; set;}
        public bool   include_shuffle_instead_of_quick_mix{get; set;}
        public bool   include_recommendations{get; set;}
        public bool   include_explanations{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_boolean_member("includeStationArtUrl", include_station_art_url);
            root.set_string_member("stationArtSize", station_art_size);
            root.set_boolean_member("includeAdAttributes", include_ad_attributes);
            root.set_boolean_member("includeStationSeeds", include_station_seeds);
            root.set_boolean_member("includeShuffleInsteadOfQuickMix", include_shuffle_instead_of_quick_mix);
            root.set_boolean_member("includeRecommendations", include_recommendations);
            root.set_boolean_member("includeExplanations", include_explanations);
        }
    }
    
    /**
     * Gets account usage information
     *
     * This request has no parameters.
     */
    public class GetUsageInfo : panda.request.Request{
        construct{ METHOD_NAME="user.getUsageInfo"; }
        
        public override void pack(ref Json.Object root){}
    }
    
    /**
     * Sets the stations to be used in a QuickMix. The response contains no data.
     */
    public class SetQuickMix : panda.request.Request{
        construct{ METHOD_NAME="user.setQuickMix"; }
        
        public string[] quick_mix_station_ids{get; set;}
        
        public override void pack(ref Json.Object root){
            var obj = new Json.Array();
            foreach(string s in quick_mix_station_ids)
                obj.add_string_element(s);
            root.set_array_member("quickMixStationIds", obj);
        }
    }
    
    /**
     * Bans a song from all stations for one month
     */
    public class SleepSong : panda.request.Request{
        construct{ METHOD_NAME="user.sleepSong"; }
        
        public string track_token{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_string_member("trackToken", track_token);
        }
    }
    
    /**
     * Starts a complimentary Pandora One trial. It is unknown what constitutes a valid sponsor at this time,
     * and as such this method will always fail.
     */
    public class StartComplimentaryTrial : panda.request.Request{
        construct{ METHOD_NAME="user.startComplimentaryTrial"; }
        
        public string complimentary_sponsor{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_string_member("complimentarySponsor", complimentary_sponsor);
        }
    }
    
    /**
     * Validates a username. Should be used before user.createUser and optionally before auth.userLogin.
     *
     * This method requires a valid partner login attempt.
     */
    public class ValidateUsername : panda.request.Request{
        construct{ METHOD_NAME="user.validateUsername"; }
        
        public string username{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_string_member("username", username);
        }
    }
}