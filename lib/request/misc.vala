using Json;

namespace panda.request{
    namespace music{

        /**
         * Submits a text search for music against track and artist names.
         */
        public class Search : panda.request.Request{
            construct{ METHOD_NAME="music.search"; }
            
            public string text{get; set;}
            
            public override void pack(ref Json.Object root){
                root.set_string_member("text", text);
            }
        }
    }

    namespace bookmark{

        /**
         * Add the artist of a track to your bookmarks. The implementation to deelete an artist is unknown at this time.
         */
        public class AddArtistBookmark : panda.request.Request{
            construct{ METHOD_NAME="bookmark.addArtistBookmark"; }
            
            public string track_token{get; set;}
            
            public override void pack(ref Json.Object root){
                root.set_string_member("trackToken", track_token);
            }
        }
        
        /**
         * Add the artist of a track to your bookmarks. The implementation to deelete an artist is unknown at this time.
         */
        public class AddSongBookmark : panda.request.Request{
            construct{ METHOD_NAME="bookmark.addSongBookmark"; }
            
            public string track_token{get; set;}
            
            public override void pack(ref Json.Object root){
                root.set_string_member("trackToken", track_token);
            }
        }
    }
    
    namespace test{

        /**
         * Check whether Pandora is available in the connecting client’s country, based on geoip database.
         * This is not strictly required since Partner login enforces this restriction. The request has no parameters.
         */
        public class CheckLicensing : panda.request.Request{
            construct{ METHOD_NAME="test.checkLicensing"; }
            
            public override void pack(ref Json.Object root){}
        }
    }
    
    namespace track{
        
        /**
         * Get an incomplete list of attributes assigned to song by Music Genome Project.
         */
        public class ExplainTrack : panda.request.Request{
            construct{ METHOD_NAME="track.explainTrack"; }
            
            public string track_token{get; set;}
            
            public override void pack(ref Json.Object root){
                root.set_string_member("trackToken", track_token);
            }
        }
    }
}