using Json;

namespace panda.request.station{

    /**
     * Adds feedback about a track to the specified station.
     *
     * Songs can be “loved” or “banned”. Both influence the music played on the station.
     * Banned songs are never played again on this particular station.
     */
    public class AddFeedback : panda.request.Request{
        construct{ METHOD_NAME="station.addFeedback"; }
        
        public string station_token{get; set;}
        public string track_token{get; set;}
        public bool   is_positive{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_string_member("stationToken", station_token);
            root.set_string_member("trackToken", track_token);
            root.set_boolean_member("isPositive", is_positive);
        }
    }

    /**
     * Removes feedback about a track from the specified station.
     *
     * Feedback added by station.addFeedback can be removed from the station.
     * This method does not return data?
     */
    public class DeleteFeedback : panda.request.Request{
        construct{ METHOD_NAME="station.addFeedback"; }
        
        public string feedback_id{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_string_member("feedbackId", feedback_id);
        }
    }

    /**
     * Adds variety to a station.
     *
     * Search results can be used to add new seeds to an existing station.
     */
    public class AddMusic : panda.request.Request{
        construct{ METHOD_NAME="station.addMusic"; }
        
        public string station_token{get; set;}
        public string music_token{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_string_member("stationToken", station_token);
            root.set_string_member("musicToken", music_token);
        }
    }

    /**
     * Removes feedback about a track from the specified station.
     *
     * Feedback added by station.addFeedback can be removed from the station.
     * This method does not return data?
     */
    public class DeleteMusic : panda.request.Request{
        construct{ METHOD_NAME="station.deleteMusic"; }
        
        public string seed_id{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_string_member("seedId", seed_id);
        }
    }

    /**
     * Deletes a station
     *
     * This method does not return data?
     */
    public class DeleteStation : panda.request.Request{
        construct{ METHOD_NAME="station.deleteStation"; }
        
        public string station_token{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_string_member("stationToken", station_token);
        }
    }

    /**
     * Gets the checksum of the Genre Stations. Used for checking if they have changed.
     */
    public class GetGenreStationsChecksum : panda.request.Request{
        construct{ METHOD_NAME="station.deleteMusic"; }
        
        public bool include_genre_category_ad_url{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_boolean_member("includeGenreCategoryAdUrl", include_genre_category_ad_url);
        }
    }

    /**
     * Gets a list of the user's stations. If specified, the additional audio url's are comma seperated.
     *
     * Valid values for additionalAudioUrl are:
     *
     * HTTP_40_AAC_MONO
     * HTTP_64_AAC
     * HTTP_32_AACPLUS
     * HTTP_64_AACPLUS
     * HTTP_24_AACPLUS_ADTS
     * HTTP_32_AACPLUS_ADTS
     * HTTP_64_AACPLUS_ADTS
     * HTTP_128_MP3
     * HTTP_32_WMA
     *
     * Usually a playlist contains four tracks.
     */
    public class GetPlaylist : panda.request.Request{
        construct{
            METHOD_NAME="station.getPlaylist";
            TLS = true;
        }
        
        public string station_token{get; set;}
        public string additional_audio_url{get; set;}
        public bool station_is_starting{get; set;}
        public bool include_track_length{get; set;}
        public bool include_audio_token{get; set;}
        public bool xplatform_ad_capable{get; set;}
        public bool include_audio_receipt_url{get; set;}
        public bool include_backstage_ad_url{get; set;}
        public bool include_sharing_ad_url{get; set;}
        public bool include_social_ad_url{get; set;}
        public bool include_competitive_sep_indicatior{get; set;}
        public bool include_complete_playlist{get; set;}
        public bool include_track_options{get; set;}
        public bool audio_ad_pod_capable{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_string_member("stationToken", station_token);
            root.set_string_member("additionalAudioUrl", additional_audio_url);
            root.set_boolean_member("stationIsStarting", station_is_starting);
            root.set_boolean_member("includeTrackLength", include_track_length);
            root.set_boolean_member("includeAudioToken", include_audio_token);
            root.set_boolean_member("xplatformAdCapable", xplatform_ad_capable);
            root.set_boolean_member("includeAudioReceiptUrl", include_audio_receipt_url);
            root.set_boolean_member("includeBackstageAdUrl", include_backstage_ad_url);
            root.set_boolean_member("includeSharingAdUrl", include_sharing_ad_url);
            root.set_boolean_member("includeSocialAdUrl", include_social_ad_url);
            root.set_boolean_member("includeCompetitiveSepindicator", include_competitive_sep_indicatior);
            root.set_boolean_member("includeCompletePlaylist", include_complete_playlist);
            root.set_boolean_member("includeTrackOptions", include_track_options);
            root.set_boolean_member("audioAdPodCapable", audio_ad_pod_capable);
        }
    }
    
    /**
     * Gets extended information about the specified station, including seeds and feedback.
     */
    public class GetStation : panda.request.Request{
        construct{ METHOD_NAME="station.getStation"; }
        
        public string station_token{get; set;}
        public bool   include_extended_attributes{get; set;}
        
        public override void pack(ref Json.Object root){ 
            root.set_string_member("stationToken", station_token);
            root.set_boolean_member("includeExtendedAttributes", include_extended_attributes);
        }
    }
    
    /**
     * Renames the specified station
     */
    public class RenameStation : panda.request.Request{
        construct{ METHOD_NAME="station.renameStation"; }
        
        public string station_token{get; set;}
        public string station_name{get; set;}
        
        public override void pack(ref Json.Object root){ 
            root.set_string_member("stationToken", station_token);
            root.set_string_member("stationName", station_name);
        }
    }
    
    /**
     * Shares a station with the specified email addresses.
     */
    public class ShareStation : panda.request.Request{
        construct{ METHOD_NAME="station.shareStation"; }
        
        public string station_id{get; set;}
        public string station_token{get; set;}
        public string[] emails{get; set;}
        
        public override void pack(ref Json.Object root){ 
            root.set_string_member("stationId", station_id);
            root.set_string_member("stationToken", station_token);
            
            var obj = new Json.Array();
            foreach(string s in emails)
                obj.add_string_element(s);
            root.set_array_member("emails", obj);
        }
    }
    
    /**
     * Transforms a shared station so that it can be changed.
     *
     * Stations created by other users are added as reference to the user’s station list.
     * These stations cannot be modified (i.e. rate tracks) unless transformed.
     */
    public class TransformSharedStation : panda.request.Request{
        construct{ METHOD_NAME="station.transformSharedStation"; }
        
        public string station_token{get; set;}
        
        public override void pack(ref Json.Object root){ 
            root.set_string_member("stationToken", station_token);
        }
    }
}