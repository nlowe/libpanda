using Json;

namespace panda.request.auth{

    /**
     * This request additionally serves as API version validation, time synchronization and
     * endpoint detection and must be sent over a TLS-encrypted link. The POST body however is not encrypted.
     */
    public class PartnerLogin : panda.request.Request{
        construct{
            METHOD_NAME = "auth.partnerLogin";
            TLS         = true;
            ENCRYPTED   = false;
        }
        
        public string username{get; set;}
        public string password{get; set;}
        public string device_model{get; set;}
        public string version{get; set;}
        public bool   include_urls{get; set;}
        public bool   return_device_type{get; set;}
        public bool   return_update_prompt_versions{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_string_member("username", username);
            root.set_string_member("password", password);
            root.set_string_member("deviceModel", device_model);
            root.set_string_member("version", version);
            root.set_boolean_member("includeUrls", include_urls);
            root.set_boolean_member("returnDeviceType", return_device_type);
            root.set_boolean_member("returnUpdatePromptVersions", return_update_prompt_versions);
        }
    }

    /**
     * This request must be sent over a TLS-encrypted link. It authenticates the Pandora user by sending his
     * username, usually his email address, and password as well as the partnerAuthToken obtained by Partner login.
     *
     * Additional response data can be requested by setting flags listed below.
     */
    public class UserLogin : panda.request.Request{
        construct{
            METHOD_NAME = "auth.userLogin";
            TLS         = true;
        }
        
        public string login_type{get; set; default="user";}
        public string username{get; set;}
        public string password{get; set;}
        //public string partner_auth_token{get; set;}
        public bool   return_genre_stations{get; set;}
        public bool   return_capped{get; set;}
        public bool   include_pandora_one_info{get; set;}
        public bool   include_demographics{get; set;}
        public bool   include_ad_attributes{get; set;}
        public bool   return_station_list{get; set;}
        public bool   include_station_art_url{get; set;}
        public bool   include_station_seeds{get; set;}
        public bool   include_shuffle_instead_of_quick_mix{get; set;}
        public string station_art_size{get; set; default="W130H130";}
        public bool   return_collect_track_lifetime_stats{get; set;}
        public bool   return_is_subscriber{get; set;}
        public bool   xplatform_ad_capable{get; set;}
        public bool   complimentary_sponsor_supported{get; set;}
        public bool   include_subscription_expiration{get; set;}
        public bool   return_has_used_trial{get; set;}
        public bool   return_userstate{get; set;}
        public bool   include_account_message{get; set;}
        public bool   include_user_webname{get; set;}
        public bool   include_listening_hours{get; set;}
        public bool   include_facebook{get; set;}
        public bool   include_twitter{get; set;}
        public bool   include_daily_skip_limit{get; set;}
        public bool   include_skip_delay{get; set;}
        public bool   include_googleplay{get; set;}
        public bool   include_show_user_recommendations{get; set;}
        public bool   include_advertiser_attributes{get; set;}
        
        public override void pack(ref Json.Object root){
            root.set_string_member("loginType", login_type);
            root.set_string_member("username", username);
            root.set_string_member("password", password);
            //root.set_string_member("partnerAuthToken", partner_auth_token);
            root.set_boolean_member("returnGenreStations", return_genre_stations);
            root.set_boolean_member("returnCapped", return_capped);
            root.set_boolean_member("includePandoraOneInfo", include_pandora_one_info);
            root.set_boolean_member("includeDemographics", include_demographics);
            root.set_boolean_member("includeAdAttributes", include_ad_attributes);
            root.set_boolean_member("returnStationList", return_station_list);
            root.set_boolean_member("includeStationArtUrl", include_station_art_url);
            root.set_boolean_member("includeStationSeeds", include_station_seeds);
            root.set_boolean_member("includeShuffleInsteadOfQuickMix", include_shuffle_instead_of_quick_mix);
            root.set_string_member("stationArtSize", station_art_size);
            root.set_boolean_member("returnCollectTrackLifetimeStats", return_collect_track_lifetime_stats);
            root.set_boolean_member("returnIsSubscriber", return_is_subscriber);
            root.set_boolean_member("xplatformAdCapable", xplatform_ad_capable);
            root.set_boolean_member("complimentarySponsorSupported", complimentary_sponsor_supported);
            root.set_boolean_member("includeSubscriptionExpiration", include_subscription_expiration);
            root.set_boolean_member("returnHasUsedTrial", return_has_used_trial);
            root.set_boolean_member("returnUserstate", return_userstate);
            root.set_boolean_member("includeAccountMessage", include_account_message);
            root.set_boolean_member("includeuserWebname", include_user_webname);
            root.set_boolean_member("includeListeningHours", include_listening_hours);
            root.set_boolean_member("includeFacebook", include_facebook);
            root.set_boolean_member("includeTwitter", include_twitter);
            root.set_boolean_member("includeDailySkipLimit", include_daily_skip_limit);
            root.set_boolean_member("includeSkipDelay", include_skip_delay);
            root.set_boolean_member("includeGoogleplay", include_googleplay);
            root.set_boolean_member("includeShowUserRecommendations", include_show_user_recommendations);
            root.set_boolean_member("includeAdvertiserAttributes", include_advertiser_attributes);
        }
    }
}