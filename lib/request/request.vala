using Gee;
using Soup;
using Json;

/**
 * A request interface that includes all of the 
 */
public abstract class panda.request.Request : GLib.Object{

    public string METHOD_NAME{get; protected set; default="UNDEFINED";}
    public bool   TLS{get; protected set; default=false;}
    public bool   ENCRYPTED{get; protected set; default=true;}

    /**
     * Packs the parameters of the request into the specified json object. Only pack
     * the unique parameters, the library will take care of the common ones.
     */
    public abstract void pack(ref Json.Object root);
}