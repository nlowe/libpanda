using Gee;

/**
 * Keeps track of the playlist, including upcomming tracks and history
 */
public class panda.Playlist : GLib.Object{

    /** When the current playlist size is below this number, we should request additional tracks */
    public uint PLAYLIST_AUTOGROW_THRESHOLD{get; set; default=1;}
    /** When the history should be trimmed */
    public uint HISTORY_AUTOTRIM_THRESHOLD{get; set; default=25;}

    /** The station that this playlist belongs to */
    public weak Station parent{get; private set;}

    /** A collection of tracks waiting to be played. Should never be manipulated directly */
    public ArrayList<Track> up_next{get; private set;}
    /** The currently selected track */
    public Track? current_track{get; private set;}
    /** The previously played or skipped tracks */
    public ArrayList<Track> history{get; private set;}
    
    public enum PLAYLIST_CHANGE_OPERATION{ADD,CURRENT_TRACK_CHANGED,TRIM}
    /** Raised when the playlist has changed*/
    public signal void playlist_changed(PLAYLIST_CHANGE_OPERATION op, Track new_track, Track? old_track);

    public Playlist(Station parent){
        this.parent = parent;
        
        up_next = new ArrayList<Track>();
        history = new ArrayList<Track>();
    }
    
    /**
     * Appends a new track to the upcomming tracks list
     */
    public void push(Track t){
        up_next.add(t);
        playlist_changed(PLAYLIST_CHANGE_OPERATION.ADD, t, null);
    }
    
    /**
     * Indexer support for the playlist. Negative values represent upcomming tracks
     * (with more negative being further away from being played),
     * zero represents the current track, and positive values represent past tracks
     * (with more positive being furhter away from now)
     */
    public Track? @get(int index){
        if(index == 0){
            return current_track;
        }else if(index < 0){
            index *= -1;
            if(index > up_next.size){
                return null;
            }else{
                return up_next[index];
            }
        }else{
            if(index > history.size){
                return null;
            }else{
                return history[index];
            }
        }
    }
    
    /**
     * Advances the playlist by one
     */
    public Track advance(){
        if(current_track != null){
            history.insert(0,current_track);
        }
        current_track = up_next[0];
        //TODO: History Trimming
        playlist_changed(PLAYLIST_CHANGE_OPERATION.CURRENT_TRACK_CHANGED, history[0], current_track);
        return current_track;
    }
    
    /**
     * Provides an array of tracks from this playlist in the following order
     * Up Next[last->first], Current Track, History[newer->older]. It is preferred
     * to listen on the playlist_changed event instead of collecting a zip of the list
     * frequently.
     */
    public Track[] zip(){
        Track[] tracks = new Track[track_count()];
        int index=0;
        for(; index<up_next.size; index++){
            tracks[index] = up_next[index];
        }
        
        if(current_track != null){
            tracks[++index] = current_track;
        }
        
        for(int i=0; i<history.size; i++){
            tracks[++index] = history[i];
        }
        
        return tracks;
    }
    
    /**
     * Provides an array of tracks form this playlist in the inverse order of <code>zip()</code>
     */
    public Track[] inverse_zip(){
        Track[] tracks = new Track[track_count()];
        int index=0;
        
        for(; index<history.size; index++){
            tracks[index] = history[index];
        }
        
        if(current_track != null){
            tracks[++index] = current_track;
        }
        
        for(int i=0; i<up_next.size; i++){
            tracks[++index] = up_next[i];    
        }
        
        return tracks;
    }
    
    /**
     * Grows the playlist if the PLAYLIST_AUTOGROW_THRESHOLD has been passed or forced
     */
    public async bool grow(bool force) throws PandoraError{
        if(remaining_tracks() < PLAYLIST_AUTOGROW_THRESHOLD || force){
            yield parent.fetch_new_playlist();
            //TODO: extra formats
            return true;
        }else{
            return false;
        }
    }

    /**
     * Get the amount of tracks in this playlist, current, upcomming, and past
     */
    public int track_count(){
        return (current_track == null ? 0 : 1) + history_count() + remaining_tracks();
    }
    
    /**
     * Gets the amount of tracks already played
     */
    public int history_count(){
        return history.size;
    }
    
    /**
     * Gets the amount of tracks left in the upcomming list
     */
    public int remaining_tracks(){
        return up_next.size;
    }
    
    //TODO: Trimming?
    
    /**
     * Gets an iterator that can iterate over this playlist in the following order:
     *      Upcomming (Furthest Away to Closest)
     *      Current Track (If Present)
     *      History (Least Time since played to most)
     */
    public Iterator iterator(){
        return new Iterator(this);
    }
    
    public class Iterator{
        private int index;
        private Playlist instance;
        
        public Iterator(Playlist instance){
            this.instance = instance;
            index = instance.remaining_tracks() * -1;
        }
        
        public Track next_value(){
            index++;
            return instance[index];
        }
    }
}