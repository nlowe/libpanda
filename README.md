libpanda [![Build Status](https://travis-ci.org/techwiz24/libpanda.svg?branch=master)](https://travis-ci.org/techwiz24/libpanda)
===
libpanda is an asynchronous library for interacting with the [Pandora JSON Api](https://github.com/techwiz24/pandora-apidoc). It provides abstractions of the json methods in an object-oriented way making it extremely simple to develop a client around it. The clients that use this library are responsible for the actual playing of audio, as this makes the library sound-subsystem independent.

Dependencies
---
 * libsoup
 * libjson-glib
 * libgee-0.8

Ubuntu:
```
sudo apt-get install libsoup2.4-1 libjson-glib-1.0-0
```

Required to build
---
 * vala >= 0.24
 * libsoup-dev
 * libjson-glib-dev
 * libgee-0.8-dev

Ubuntu:
```
sudo apt-get install libsoup2.4-dev libjson-glib-dev libgee-0.8-dev
```
 
Build a library
---
I'm tentatively using cmake for now:
```
mkdir build && cd build
cmake .. -DNO_VALADOC=TRUE
make && sudo make install && sudo ldconfig
```
